from django.shortcuts import render, get_object_or_404
from projects.models import Project


# Create your views here.
def list_projects(request):

    user = Project.objects.create(owner=request.id)
    project = Project.objects.all(name='Example Project', owner=user)
    context = { "project": project,} 
    return render(request, "projects/projects.html" , context)

